// pasta assert function
function AssertException(message) { this.message = message; }
AssertException.prototype.toString = function () {
  return 'AssertException: ' + this.message;
}

function assert(exp, message) {
  if (!exp) {
    alert('assertion fail: '+message);
    throw new AssertException(message);
  }
}

var dbhost = 'http://localhost:5984/';

var page = {};

// TODO add start up options
page.editpage = function() {
  // create editor
  var editor = CodeMirror.fromTextArea($('#editor').get(0), {
    mode: 'markdown',
    lineNumbers: true,
    matchBrackets: true,
    theme: "default"
  });

  editor.scrollToLine = function(line) {
    // use height of a line to calculate height in pixels
    if (typeof line === 'undefined')
      return;
    var line_height = parseInt(
      $('.CodeMirror div div:last-child pre:first-child')
        .height(), 253);
    this.scrollTo(0, (line - 1)*line_height);
    this.setCursor(line-1);
  };

  // cached selectors
  var gutter = $('#editdiv .CodeMirror-gutter');
  var switchbar = $('#switch');
  var panel = $('#panel'); 
  var editdiv = $('#editdiv');
  var renderdiv = $('#renderdiv');
  var renderwrap = $('#renderwrap'); // rendered content are here

  // states are 'edit', 'render'
  var state = 'edit';

  // storing state related constants
  var edit_context = {},
      render_context = {};

  // pointers to contexts updated when state transit happens
  var context = edit_context,
      alter  = render_context;
  
  // helpers to loadin
  function load_color() {
    // use lighter color for edit and darker color for rendering
    var gutter_color = $.color.parse(gutter.css('background-color'));
    edit_context.switch_color = gutter_color.toString();
    render_context.switch_color = gutter_color.scale('rgb', 0.9).toString();
    edit_context.picto_color = render_context.picto_color =
      $.color.parse($('.picto').css('color')).toString();

    sync_ui_color();
  }

  function sync_ui_color() {
    // update ui colors
    switchbar.css('color', context.picto_color);
    switchbar.css('background-color', context.switch_color);
    panel.css('color', context.picto_color);
    panel.css('background-color', context.switch_color);
  }

  function state_switch() {
    function to_render() {
      render_editor();
      renderdiv_slide('in');
      return true;
    }
    function to_edit() {
      renderdiv_slide('out');
      return true;
    }
    var switched = false;
    if (state == 'edit' && to_render()) {
      state = 'render';
      context = render_context;
      alter  = edit_context;
      switched = true;
    } else if (state == 'render' && to_edit()) {
      state = 'edit';
      context = edit_context;
      alter  = render_context;
      switched = true;
    } else {
      assert('invalid state');
    }

    // common behavior
    if (switched) {
      sync_ui_color();
    }
    console.log(state);

    return switched;
  }

  // ui related
  function renderdiv_slide(dir) {
    // shall not hide/show edit div since it would breaks cursor
    // position when switch back to editing, or should try
    // display:hidden
    if (dir == 'in') {
      renderdiv.animate({'left': '0%'}, 'fast');
    } else if (dir == 'out') {
      renderdiv.animate({'left': '-102%'}, 'fast', function(){
        editor.focus();
      });
    }
  }

  // content related

  // render texts in editor and send them to #renderdiv
  function render_editor() {
    renderwrap.empty();
    var blocks = markdown.toHTMLBlocks(editor.getValue());
    for (var ix = 0; ix < blocks.length; ++ix) {
      $(document.createElement('div'))
        .addClass('block-outer')
        .appendTo(renderwrap)
        .append(
            // append a edit bar
            $(document.createElement('div'))
              .addClass('block-edit')
              .data('lineNumber', blocks[ix].lineNumber)
              .css('background-color', alter.switch_color)
        )
        .append(
            $(document.createElement('div'))
              .addClass('block-content')
              .html(blocks[ix].toString())
        );
    }
  }

  // event handlers
  switchbar.mouseenter(function(event){
    if (state == 'edit')
      $(this).css('background-color', alter.switch_color);
    else if (state == 'render')
      switchbar.css('color', alter.switch_color);
  }).mouseleave(function(event){
    if (state == 'edit')
      $(this).css('background-color', context.switch_color);
    else if (state == 'render')
      switchbar.css('color', context.picto_color);
  }).click(function(event){
    state_switch();
    return false;
  });

  $('#renderwrap').on({
    mouseenter : function(event) {
      $(this).text('&'); // picto for edit
    },
    mouseleave : function(event) {
      $(this).empty();
    },
    click : function(event) {
      state_switch();
      editor.scrollToLine($(this).data('lineNumber'));
    }
  }, '.block-edit');

  // kick off
  load_color();
  document.editor = editor;
}

// login/register form which should be available on
// every page
var lrform = {};
lrform.form = $('#lrform');
lrform.login = $('#login');
lrform.register = $('#register');
lrform.smoke = $('#smoke');

lrform.reason = function(reason_str) {
  $('#reason').text(reason_str);
  return this;
};

lrform.show_register = function(show) {
  if (show) {
    this.register.show();
  } else {
    this.register.hide();
  }
  return this;
};

lrform.show_close = function(show) {
  var close = $('#lrform-close');
  if (show) {
    close.show();
  } else {
    close.hide();
  }
  return this;
};

lrform.show = function() {
  this.form.show();
  this.smoke.show();
  return this;
};

lrform.hide = function() {
  this.form.hide();
  this.smoke.hide();
  return this;
};

window.lrform = lrform;

lrform.init = function() {
  $('#lrform-close').click(function(event){
    lrform.hide();
    return false;
  });
  $('#register').submit(function(event) {
    var body_str = $(this).serialize();
    $.post('/register', body_str)
    .success(function(data) {
      alert(data);
    })
    .error(function(data) {
      var msg = $.parseJSON(data.responseText).error;
      alert(msg);
      // must default behavior of jumping
      return false;
    });
    return false;
  });

};


$(function(){
  var pageid = $('body').attr('id');
  page[pageid]();

  // general init
  lrform.init();
});
