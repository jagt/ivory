/**
 * Normalize the given path string,
 * returning a regular expression.
 *
 * An empty array should be passed,
 * which will contain the placeholder
 * key names. For example "/user/:id" will
 * then contain ["id"].
 *
 * @param  {String|RegExp|Array} path
 * @param  {Array} keys
 * @param  {Boolean} sensitive
 * @param  {Boolean} strict
 * @return {RegExp}
 * @api private
 */
var url = require('url');

// TODO NON optional trailing slash
// pasta from express.js
exports.pathRegexp = function(path, keys, sensitive, strict) {
  if (path instanceof RegExp) return path;
  if (Array.isArray(path)) path = '(' + path.join('|') + ')';
  path = path
    .concat(strict ? '' : '/?')
    .replace(/\/\(/g, '(?:/')
    .replace(/\+/g, '__plus__')
    .replace(/(\/)?(\.)?:(\w+)(?:(\(.*?\)))?(\?)?/g, function(_, slash, format, key, capture, optional){
      keys.push({ name: key, optional: !! optional });
      slash = slash || '';
      return ''
        + (optional ? '' : slash)
        + '(?:'
        + (optional ? slash : '')
        + (format || '') + (capture || (format && '([^/.]+?)' || '([^/]+?)')) + ')'
        + (optional || '');
    })
    .replace(/([\/.])/g, '\\$1')
    .replace(/__plus__/g, '(.+)')
    .replace(/\*/g, '(.*)');
  return new RegExp('^' + path + '$', sensitive ? '' : 'i');
}

exports.router = function(matches) {
  // rebuild routes to retrieve attributes
  var match_url;
  var routes = [];
  for (match_url in matches) {
      var route = { 
          keys : [],
          handler : matches[match_url]
      };
      route.re = exports.pathRegexp(match_url, route.keys);
      routes.push(route);
  }


  return function(req, res, next) {
    var pathname = url.parse(req.url).pathname;
    for (var ix in routes) {
        var route = routes[ix];
        var result = route.re.exec(pathname);
        if ( result ) {
          // matched and end
          req.param = {};
          for (var jx = 0; jx < route.keys.length; ++jx) {
              key = route.keys[jx];
              req.param[key.name] = decodeURIComponent(result[jx+1]);
          }
          return route.handler(req, res, next);
        }
    }
    // not found
    return next();
  }
}

